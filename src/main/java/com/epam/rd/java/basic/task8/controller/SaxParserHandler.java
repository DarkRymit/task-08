package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import static com.epam.rd.java.basic.task8.controller.TagConstants.*;

public class SaxParserHandler extends DefaultHandler{

    private List<Flower> flowers = null;
    private String currentElement = null;
    private Flower flower = null;
    private VisualParameters vp = null;
    private GrowingTips gt = null;

    @Override
    public void startDocument() {
        flowers = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElement = qName;
        switch (currentElement) {
            case FLOWER_TAG:
                flower = new Flower();
                break;
            case VISUAL_PARAMETERS_TAG:
                vp = new VisualParameters();
                break;
            case GROWING_TIPS_TAG:
                gt = new GrowingTips();
                break;
            case AVE_LEN_FLOWER_TAG:
                AveLenFlower aveLenFlower = new AveLenFlower();
                aveLenFlower.setMeasure(attributes.getValue(MEASURE_TAG));
                vp.setAveLenFlower(aveLenFlower);
                break;
            case TEMPRETURE_TAG:
                GrowingTips.Temperature temperature = new GrowingTips.Temperature();
                temperature.setMeasure(attributes.getValue(MEASURE_TAG));
                gt.setTemperature(temperature);
                break;
            case LIGHTING_TAG:
                gt.setLightRequiring(LightRequiring.valueOf(attributes.getValue(LIGHT_REQUIRING_TAG)));
                break;
            case WATERING_TAG:
                Watering watering = new Watering();
                watering.setMeasure(attributes.getValue(MEASURE_TAG));
                gt.setWatering(watering);
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        currentElement = null;
        switch (qName) {
            case FLOWER_TAG:
                flowers.add(flower);
                flower = null;
                break;
            case VISUAL_PARAMETERS_TAG:
                flower.setVisualParameters(vp);
                vp = null;
                break;
            case GROWING_TIPS_TAG:
                flower.setGrowingTips(gt);
                gt = null;
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String data = new String(ch, start, length).strip();

        if(currentElement == null) return;

        switch (currentElement) {
            case NAME_TAG:
                flower.setName(data);
                break;
            case SOIL_TAG:
                flower.setSoil(data);
                break;
            case ORIGIN_TAG:
                flower.setOrigin(data);
                break;
            case STEM_COLOUR_TAG:
                vp.setStemColor(data);
                break;
            case LEAF_COLOUR_TAG:
                vp.setLeafColour(data);
                break;
            case AVE_LEN_FLOWER_TAG:
                vp.getAveLenFlower().setContent(Integer.parseInt(data));
                break;
            case TEMPRETURE_TAG:
                gt.getTemperature().setContent(Integer.parseInt(data));
                break;
            case WATERING_TAG:
                gt.getWatering().setContent(Integer.parseInt(data));
                break;
            case MULTIPLYING_TAG:
                flower.setMultiplying(data);
                break;
        }
    }

    public List<Flower> getResult(){
        return flowers;
    }
}
