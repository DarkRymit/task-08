package com.epam.rd.java.basic.task8.entity;

public class GrowingTips {
    private Temperature temperature;
    private LightRequiring lightRequiring;
    private Watering watering;

    public GrowingTips() {
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public LightRequiring getLightRequiring() {
        return lightRequiring;
    }

    public void setLightRequiring(LightRequiring lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    public static class Temperature {
        private int content;
        private String measure;

        public Temperature() {
        }

        public int getContent() {
            return content;
        }

        public void setContent(int content) {
            this.content = content;
        }

        public String getMeasure() {
            return measure;
        }

        public void setMeasure(String measure) {
            this.measure = measure;
        }

        @Override
        public String toString() {
            return "Temperature{" +
                    "content=" + content +
                    ", measure='" + measure + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", lightRequiring=" + lightRequiring +
                ", watering=" + watering +
                '}';
    }
}
